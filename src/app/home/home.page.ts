import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  tempObj;

  constructor(public http:HttpClient) {
    this.callApi();
  }

  callApi() {
    this.http.get('https://us-central1-experiment-apis.cloudfunctions.net/fc/task4').subscribe(res=>  {
      this.tempObj=res;
      console.log(this.tempObj);
    }),
    err=>{
      console.log(err);
    }
    
  }

}
